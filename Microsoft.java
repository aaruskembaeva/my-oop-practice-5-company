package Ass5;

public class Microsoft implements Organization {


    private String nameOfOrganization;

    public Microsoft (String nameOfOrganization)
    {
        this.nameOfOrganization = nameOfOrganization;
    }


    @Override
    public String getNameOfOrganization() {
        return nameOfOrganization;
    }


    @Override
    public void setNameOfOrganization(String nameOfOrganization) {
        this.nameOfOrganization=nameOfOrganization;
    }
}
