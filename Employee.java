package Ass5;
public class Employee extends Personal{
    //Write a all the instance variables to need
    private String name;
    private String sex;
    private String jobTitle;
    private Organization organization;
    private double salary;

    //write a getter and setter for all the instance variables
    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public double getSalary(){
        return salary;
    }
    public void setSalary(double salary){
        this.salary=salary;
    }
    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSex() {
        return sex;
    }


    @Override
    public void setName(String name) {
        this.name=name;
    }

    @Override
    public void setSex(String sex) {
        if(sex.equalsIgnoreCase("male")||sex.equalsIgnoreCase("female")) {
            this.sex = sex;
        }
        else {
            throw new IllegalArgumentException("This is not valid sex");
        }
        //checking here to see if this is a valid argument
    }

    public double getMonthlySalary(){
        return this.salary/12;
    }

    @Override
    //toString method returns turns all of the data elements
    public String toString() {
        String message = super.toString();
        message = "Name:" +getName()+" . Sex:"+getSex()+" , Job Title: " + getJobTitle() +
                ", Organization: " + getOrganization().getNameOfOrganization()+", Salary:"+getSalary();
        return message;
    }

}
