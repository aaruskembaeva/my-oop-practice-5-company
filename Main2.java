package Ass5;

import java.util.ArrayList;
import java.util.List;
//Also i implemented the  methods from the interface and abstract classes
public class Main2 {
    public static void main(String []args) {
        //that`s test program to test all the methods defined in Employee.
        System.out.println("Personal of Organization:"+"\n");
        List<Personal> employee = new ArrayList<Personal>();
        Personal anEmployee = new Employee();
        anEmployee.setName("Nina Alesci");
        anEmployee.setSex("Female");
        ((Employee) anEmployee).setJobTitle("Sr. Software Engineer");
        ((Employee) anEmployee).setOrganization(new Google("Google"));
        ((Employee) anEmployee).setSalary(40000.0);
        employee.add(anEmployee);
        System.out.println(anEmployee);
        System.out.println("The monthly salary: "+((Employee) anEmployee).getMonthlySalary()+"\n");

        anEmployee = new Employee();
        anEmployee.setName("Jane Doe");
        anEmployee.setSex("Female");
        ((Employee) anEmployee).setJobTitle("Sr. Software Engineer");
        ((Employee) anEmployee).setOrganization(new Google("Microsoft"));
        ((Employee) anEmployee).setSalary(50000.0);
        employee.add(anEmployee);
        System.out.println(anEmployee);
        System.out.println("The monthly salary: "+((Employee) anEmployee).getMonthlySalary());

    }
}
