package Ass5;

public class Google implements Organization {
    private String nameOfOrganization;


    public Google (String nameOfOrganization) {
        this.nameOfOrganization = nameOfOrganization;
    }


    @Override
    public String getNameOfOrganization() {
        return nameOfOrganization;
    }


    @Override
    public void setNameOfOrganization(String nameOfOrganization) {
        this.nameOfOrganization = nameOfOrganization;
    }
}
