package Ass5;

public abstract class Personal {
    public abstract String getName();
    public abstract String getSex();
    public abstract void setName(String name);
    public abstract void setSex(String sex);

    @Override
    public String toString(){
        return "Name:" + getName() + " ," + "Sex:" + getSex() + "\n";
    }

}

