package Ass5;

// i develop a system that can track employee information for two Organizations:Google and Microsoft.
// The Employee information you must track is as follows:
// Name
//Sex
//Job Title
//Organization
//Salary
//monthly salary

public interface Organization {

    public String getNameOfOrganization();
    public void setNameOfOrganization(String nameOfOrganization);
}